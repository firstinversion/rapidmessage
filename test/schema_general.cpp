#include <catch2/catch.hpp>
#include <rapidmessage/object.hpp>
#include <rapidmessage/schema.hpp>

TEST_CASE("schema_general: simple validation", "[schema_general]") {
    namespace schema = rapidmessage::schema;

    auto sch = schema::compile(
        schema::object(schema::property("x", schema::number{}), schema::property("y", schema::number{})));

    const char* example_correct_data = R"({
        "x": 25.26,
        "y": 85.29
    })";

    const char* example_incorrect_data = R"({
        "x": "howdy",
        "y": 85.29
    })";

    rapidjson::Document correct_document;
    rapidjson::Document incorrect_document;

    correct_document.Parse(example_correct_data);
    incorrect_document.Parse(example_incorrect_data);

    REQUIRE(!correct_document.HasParseError());
    REQUIRE(!incorrect_document.HasParseError());

    rapidjson::SchemaValidator correct_validator(sch);
    rapidjson::SchemaValidator incorrect_validator(sch);

    REQUIRE(correct_document.Accept(correct_validator));
    REQUIRE(!incorrect_document.Accept(incorrect_validator));
}

RAPID_MESSAGE(point) {
public:
    RAPID_MESSAGE_CONSTRUCTORS(point);

    RAPID_PROP_BYVAL(float, x);
    RAPID_PROP_BYVAL(float, y);

    const rapidjson::SchemaDocument& schema() const override {
        namespace s = rapidmessage::schema;
        static const auto schema_document =
            s::compile(s::object(s::property("x", s::number{}), s::property("y", s::number{})));
        return schema_document;
    }
};

TEST_CASE("schema_general: as base_message", "[schema_general]") {
    point valid_point(R"({ "x": 25.26, "y": 85.29 })");
    REQUIRE(valid_point.x() == 25.26f);
    REQUIRE(valid_point.y() == 85.29f);

    try {
        point invalid_point(R"({ "x": "howdy", "y": 85.29 })");
        invalid_point.validate_throw();
        assert(false);
    } catch (const rapidmessage::validation_error& e) {}
}

TEST_CASE("schema_general: as base_message with nothrow validation", "[schema_general]") {
    point invalid_point(R"({ "x": "howdy", "y": 85.29 })");
    REQUIRE(invalid_point.validation_error().has_value());
}
