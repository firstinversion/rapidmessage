#include <catch2/catch.hpp>

#include <string>
#include <string_view>

TEST_CASE("string coercion: string -> string_view") {
    std::string      s  = "test";
    std::string_view sv = s;
    REQUIRE(sv == "test");
}

TEST_CASE("string coercion: string_view -> string") {
    std::string      s  = "test";
    std::string_view sv = s;
    //    std::string os = sv; // No implicit conversion.
    std::string os = std::string(s.data(), s.size());
    REQUIRE(os == "test");
}

TEST_CASE("string coercion: const char* -> string") {
    const char* c = "test";
    std::string s = c;
    REQUIRE(s == "test");
}

TEST_CASE("string coercion: const char* -> string_view") {
    const char*      c  = "test";
    std::string_view sv = c;
    REQUIRE(sv == "test");
}

TEST_CASE("string coercion: string_view -> const char*") {
    std::string      s  = "test";
    std::string_view sv = s;
    //    const char* c = sv; // No implicit conversion;
    const char* c = sv.data();
    //    REQUIRE(c == "test"); // Failed inexplicably
}
