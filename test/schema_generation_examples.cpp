#include <catch2/catch.hpp>
#include <fmt/format.h>
#include <rapidjson/document.h>
#include <rapidjson/schema.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

namespace schema_generator_examples {

    class schema_component {
    public:
        virtual ~schema_component() = default;

        virtual void apply(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator) const = 0;
    };

    class schema_object : public schema_component {
    public:
        using field = std::pair<std::string, std::shared_ptr<schema_component>>;

        schema_object(std::initializer_list<field> fields)
            : _fields(fields) {}

        void apply(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator) const override {
            value.SetObject();
            value.AddMember("type", "object", allocator);

            rapidjson::Value properties(rapidjson::kObjectType);
            std::for_each(_fields.begin(), _fields.end(), [&](const field& field) {
                rapidjson::Value field_value;
                field.second->apply(field_value, allocator);
                properties.AddMember(
                    rapidjson::Value().SetString(field.first.data(), allocator), field_value, allocator);
            });
            value.AddMember("properties", properties, allocator);
        }

    private:
        std::vector<field> _fields;
    };

    class schema_number : public schema_component {
    public:
        schema_number() {}

        void apply(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator) const override {
            value.SetObject();
            value.AddMember("type", "number", allocator);
        }
    };

    std::shared_ptr<schema_number> number() { return std::make_shared<schema_number>(); }

    const char* example_schema = R"({
    "type": "object",
    "properties": {
        "x": {
            "type": "number"
        },
        "y": {
            "type": "number"
        }
    }
})";

    const char* example_correct_data = R"({
    "x": 25.26,
    "y": 85.29
})";

    const char* example_incorrect_data = R"({
    "x": "howdy",
    "y": 85.29
})";

    rapidjson::Document compile_schema_components(const schema_component* root) {
        rapidjson::Document document;
        root->apply(document, document.GetAllocator());
        return document;
    }

    rapidjson::SchemaDocument compile(const schema_component* root) {
        return rapidjson::SchemaDocument(compile_schema_components(root));
    }

    rapidjson::StringBuffer render_document(const rapidjson::Document& document) {
        rapidjson::StringBuffer buffer;
        rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
        document.Accept(writer);
        return buffer;
    }

}  // namespace schema_generator_examples

TEST_CASE("schema_generation_example: schema literal", "[schema_generation_example]") {
    rapidjson::Document schema_document;
    rapidjson::Document correct_data_document;
    rapidjson::Document incorrect_data_document;

    schema_document.Parse(schema_generator_examples::example_schema);
    correct_data_document.Parse(schema_generator_examples::example_correct_data);
    incorrect_data_document.Parse(schema_generator_examples::example_incorrect_data);

    REQUIRE(!correct_data_document.HasParseError());
    REQUIRE(!incorrect_data_document.HasParseError());

    rapidjson::SchemaDocument  schema(schema_document);
    rapidjson::SchemaValidator correct_validator(schema);
    rapidjson::SchemaValidator incorrect_validator(schema);

    REQUIRE(correct_data_document.Accept(correct_validator));
    REQUIRE(!incorrect_data_document.Accept(incorrect_validator));
}

TEST_CASE("schema_generation_example: schema builder", "[schema_generation_example]") {
    using namespace schema_generator_examples;

    rapidjson::Document correct_data_document;
    rapidjson::Document incorrect_data_document;

    correct_data_document.Parse(schema_generator_examples::example_correct_data);
    incorrect_data_document.Parse(schema_generator_examples::example_incorrect_data);

    REQUIRE(!correct_data_document.HasParseError());
    REQUIRE(!incorrect_data_document.HasParseError());

    schema_object             sobj{{"x", number()}, {"y", number()}};
    rapidjson::Document       schema_document = compile_schema_components(&sobj);
    rapidjson::SchemaDocument schema(schema_document);

    rapidjson::SchemaValidator correct_validator(schema);
    rapidjson::SchemaValidator incorrect_validator(schema);

    REQUIRE(correct_data_document.Accept(correct_validator));
    REQUIRE(!incorrect_data_document.Accept(incorrect_validator));
}
