#include <catch2/catch.hpp>

#include <fmt/format.h>
#include <rapidmessage/array.hpp>
#include <rapidmessage/object.hpp>
#include <rapidmessage/schema.hpp>

namespace test::objects {

    const char* data_root_address = R"({
    "line_1": "123 W. Drive St.",
    "line_2": "#123",
    "city": "Kansas City",
    "state": "MO",
    "zip": 64111
})";

    const char* data_sub_address = R"({
    "address": {
        "line_1": "123 W. Drive St.",
        "line_2": "#123",
        "city": "Kansas City",
        "state": "MO",
        "zip": 64111
    }
})";

    const char* data_array_field = R"({
    "points": [
        {"x": 34, "y": 47},
        {"x": 542, "y": 234}
    ]
})";

    RAPID_MESSAGE(address) {
    public:
        RAPID_MESSAGE_CONSTRUCTORS(address);
        RAPID_MESSAGE_DEFAULT_VALIDATION;

        RAPID_PROP_STRING(line_1);
        RAPID_PROP_STRING(line_2);
        RAPID_PROP_STRING(city);
        RAPID_PROP_STRING(state);
        RAPID_PROP_BYVAL(uint32_t, zip);
    };

    RAPID_MESSAGE(person) {
    public:
        RAPID_OBJECT(p_address) {
        public:
            RAPID_OBJECT_CONSTRUCTORS(p_address);

            RAPID_PROP_STRING(line_1);
            RAPID_PROP_STRING(line_2);
            RAPID_PROP_STRING(city);
            RAPID_PROP_STRING(state);
            RAPID_PROP_BYVAL(uint32_t, zip);
        };

        RAPID_MESSAGE_CONSTRUCTORS(person);
        RAPID_MESSAGE_DEFAULT_VALIDATION;

        RAPID_PROP_BYVAL(p_address, address);
    };

    RAPID_MESSAGE(geometry) {
    public:
        RAPID_OBJECT(point) {
        public:
            RAPID_OBJECT_CONSTRUCTORS(point);

            RAPID_PROP_BYVAL(int32_t, x);
            RAPID_PROP_BYVAL(int32_t, y);
        };

        RAPID_MESSAGE_CONSTRUCTORS(geometry);
        RAPID_MESSAGE_DEFAULT_VALIDATION;

        RAPID_PROP_BYREF(rapidmessage::array<geometry::point>, points);
    };

    RAPID_MESSAGE(half_point) {
    public:
        RAPID_MESSAGE_CONSTRUCTORS(half_point);
        RAPID_MESSAGE_DEFAULT_VALIDATION;

        RAPID_PROP_BYVAL(int32_t, x);
        RAPID_PROP_BYVAL(std::optional<int32_t>, y);
    };

    RAPID_MESSAGE(part_person) {
    public:
        RAPID_MESSAGE_CONSTRUCTORS(part_person);
        RAPID_MESSAGE_DEFAULT_VALIDATION;

        RAPID_PROP_STRING(first_name);
        RAPID_PROP_OPTIONAL_STRING(last_name);
    };

}  // namespace test::objects

using namespace test::objects;

TEST_CASE("construct root from StringStream") {
    rapidjson::StringStream ss(data_root_address);
    address                 a(ss);
    REQUIRE(a.line_1() == "123 W. Drive St.");
    REQUIRE(a.line_2() == "#123");
    REQUIRE(a.city() == "Kansas City");
    REQUIRE(a.state() == "MO");
    REQUIRE(a.zip() == 64111);
}

TEST_CASE("construct root from const char*") {
    address a(data_root_address);
    REQUIRE(a.line_1() == "123 W. Drive St.");
    REQUIRE(a.line_2() == "#123");
    REQUIRE(a.city() == "Kansas City");
    REQUIRE(a.state() == "MO");
    REQUIRE(a.zip() == 64111);
}

TEST_CASE("construct sub from StringStream") {
    rapidjson::StringStream ss(data_sub_address);
    person                  p(ss);
    REQUIRE(p.address().line_1() == "123 W. Drive St.");
    REQUIRE(p.address().line_2() == "#123");
    REQUIRE(p.address().city() == "Kansas City");
    REQUIRE(p.address().state() == "MO");
    REQUIRE(p.address().zip() == 64111);
}

TEST_CASE("construct sub from const char*") {
    person p(data_sub_address);
    REQUIRE(p.address().line_1() == "123 W. Drive St.");
    REQUIRE(p.address().line_2() == "#123");
    REQUIRE(p.address().city() == "Kansas City");
    REQUIRE(p.address().state() == "MO");
    REQUIRE(p.address().zip() == 64111);
}

TEST_CASE("default root construct and render") {
    address a;
    a.line_1("123 W. Drive St.");
    a.line_2("#123");
    a.city("Kansas City");
    a.state("MO");
    a.zip(64111);

    rapidjson::Document expected;
    expected.Parse(data_root_address);
    REQUIRE(a.to_document() == expected);
}

TEST_CASE("default sub construct and render") {
    person            p;
    person::p_address a = p.address();
    a.line_1("123 W. Drive St.");
    a.line_2("#123");
    a.city("Kansas City");
    a.state("MO");
    a.zip(64111);

    rapidjson::Document expected;
    expected.Parse(data_sub_address);
    REQUIRE(p.to_document() == expected);
}

TEST_CASE("construct sub with array") {
    geometry g(data_array_field);
    REQUIRE(g.points()[0].x() == 34);
    REQUIRE(g.points()[0].y() == 47);
    REQUIRE(g.points()[1].x() == 542);
    REQUIRE(g.points()[1].y() == 234);
}

TEST_CASE("default construct and render with array") {
    geometry g;
    auto     pts = g.points();
    auto     p1  = pts.insert();
    p1.x(34);
    p1.y(47);
    auto p2 = pts.insert();
    p2.x(542);
    p2.y(234);

    rapidjson::Document expected;
    expected.Parse(data_array_field);
    REQUIRE(g.to_document() == expected);
}

TEST_CASE("set property from l-value") {
    address     a;
    std::string line1 = "123 W. East Dr.";
    a.line_1(line1);
    REQUIRE(a.line_1() == "123 W. East Dr.");
}

TEST_CASE("half_point with value") {
    half_point p1(R"({"x":23, "y": 234})");
    REQUIRE(!p1.validation_error());
    REQUIRE(p1.y().has_value());
    REQUIRE(p1.y());
    REQUIRE(p1.y() == 234);
}

TEST_CASE("half_point without optional value") {
    half_point p1(R"({"x":23})");
    REQUIRE(!p1.validation_error());
    REQUIRE(!p1.y().has_value());
}

TEST_CASE("part_person with optional value") {
    part_person p(R"({"first_name": "John", "last_name": "Doe"})");
    REQUIRE(!p.validation_error());
    REQUIRE(p.last_name() == "Doe");
}

TEST_CASE("part_person without optional value") {
    part_person p(R"({"first_name": "John"})");
    REQUIRE(!p.validation_error());
    REQUIRE(!p.last_name().has_value());
}
