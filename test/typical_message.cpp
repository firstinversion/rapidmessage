#include <catch2/catch.hpp>

#include <rapidjson/document.h>
#include <rapidjson/schema.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>
#include <string>
#include <string_view>

#include <rapidmessage/array.hpp>
#include <rapidmessage/error.hpp>

namespace typical_message {

    const char* example_schema = R"({
    "type": "object",
    "properties": {
        "id": {
            "type": "integer",
            "minimum": 0
        },
        "first_name": {
            "type": "string"
        },
        "last_name": {
            "type": "string"
        },
        "email_addresses": {
            "type": "array",
            "items": {
                "type": "string"
            }
        },
        "phone_numbers": {
            "type": "array",
            "items": {
                "type": "string"
            }
        },
        "addresses": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "line_1": {
                        "type": "string"
                    },
                    "line_2": {
                        "type": "string"
                    },
                    "city": {
                        "type": "string"
                    },
                    "state": {
                        "type": "string"
                    },
                    "zip": {
                        "type": "integer",
                        "minimum": 0
                    }
                }
            }
        }
    }})";

    const char* example_data = R"({
    "id": 3486,
    "first_name": "John",
    "last_name": "Doe",
    "email_addresses": [
        "johndoe@gmail.com",
        "jdoe@business.com"
    ],
    "phone_numbers": [
        "+1 (388) 385-2938",
        "+1 (747) 472-3957"
    ],
    "addresses": [
        {
            "line_1": "274 W. 43rd St.",
            "line_2": "#245",
            "city": "Kansas City",
            "state": "MO",
            "zip": 64111
        }
    ]})";

    class contact {
    public:
        contact()
            : _document(rapidjson::kObjectType) {}

        uint64_t id() const { return _document["id"].GetUint64(); }

        void id(uint64_t id) { _document.AddMember("id", id, _document.GetAllocator()); }

        std::string_view first_name() const {
            auto& val = _document["first_name"];
            return std::string_view(val.GetString(), val.GetStringLength());
        }

        void first_name(const std::string& first_name) {
            rapidjson::Value string;
            string.SetString(first_name.data(), first_name.size(), _document.GetAllocator());
            _document.AddMember("first_name", string, _document.GetAllocator());
        }

        std::string_view last_name() const {
            auto& val = _document["last_name"];
            return std::string_view(val.GetString(), val.GetStringLength());
        }

        void last_name(const std::string& last_name) {
            rapidjson::Value string;
            string.SetString(last_name.data(), last_name.size(), _document.GetAllocator());
            _document.AddMember("last_name", string, _document.GetAllocator());
        }

        class email_array {
        public:
            email_array(rapidjson::Value& array, rapidjson::Document& document)
                : _array(array)
                , _document(document) {}

            std::string_view operator[](size_t idx) {
                auto& val = _array[idx];
                return std::string_view(val.GetString(), val.GetStringLength());
            }

            void insert(const std::string& email) {
                rapidjson::Value string;
                string.SetString(email.data(), email.size(), _document.GetAllocator());
                _array.PushBack(string, _document.GetAllocator());
            }

        private:
            rapidjson::Value&    _array;
            rapidjson::Document& _document;
        };

        email_array email_addresses() {
            auto itr = _document.FindMember("email_addresses");
            if (itr == _document.MemberEnd()) {
                rapidjson::Value val(rapidjson::kArrayType);
                _document.AddMember("email_addresses", val, _document.GetAllocator());
                return email_array(_document["email_addresses"], _document);
            } else {
                return email_array(itr->value, _document);
            }
        }

        auto phone_numbers() {
            auto itr = _document.FindMember("phone_numbers");
            if (itr == _document.MemberEnd()) {
                rapidjson::Value val(rapidjson::kArrayType);
                _document.AddMember("phone_numbers", val, _document.GetAllocator());
                return rapidmessage::array<std::string_view>(_document["phone_numbers"], _document.GetAllocator());
            } else {
                return rapidmessage::array<std::string_view>(itr->value, _document.GetAllocator());
            }
        }

        class address {
        public:
            address(rapidjson::Value& address, rapidjson::Document::AllocatorType& allocator)
                : _address(address)
                , _allocator(allocator) {}

            std::string_view line_1() const {
                auto& val = _address["line_1"];
                return std::string_view(val.GetString(), val.GetStringLength());
            }

            void line_1(const std::string& line_1) {
                rapidjson::Value string;
                string.SetString(line_1.data(), line_1.size(), _allocator);
                _address.AddMember("line_1", string, _allocator);
            }

            std::string_view line_2() const {
                auto& val = _address["line_2"];
                return std::string_view(val.GetString(), val.GetStringLength());
            }

            void line_2(const std::string& line_2) {
                rapidjson::Value string;
                string.SetString(line_2.data(), line_2.size(), _allocator);
                _address.AddMember("line_2", string, _allocator);
            }

            std::string_view city() const {
                auto& val = _address["city"];
                return std::string_view(val.GetString(), val.GetStringLength());
            }

            void city(const std::string& city) {
                rapidjson::Value string;
                string.SetString(city.data(), city.size(), _allocator);
                _address.AddMember("city", string, _allocator);
            }

            std::string_view state() const {
                auto& val = _address["state"];
                return std::string_view(val.GetString(), val.GetStringLength());
            }

            void state(const std::string& state) {
                rapidjson::Value string;
                string.SetString(state.data(), state.size(), _allocator);
                _address.AddMember("state", string, _allocator);
            }

            uint64_t zip() const { return _address["zip"].GetUint64(); }

            void zip(uint64_t zip) { _address.AddMember("zip", zip, _allocator); }

        private:
            rapidjson::Value&                   _address;
            rapidjson::Document::AllocatorType& _allocator;
        };

        auto addresses() {
            auto itr = _document.FindMember("addresses");
            if (itr == _document.MemberEnd()) {
                rapidjson::Value array(rapidjson::kArrayType);
                _document.AddMember("addresses", array, _document.GetAllocator());
                return rapidmessage::array<address>(_document["addresses"], _document.GetAllocator());
            } else {
                return rapidmessage::array<address>(itr->value, _document.GetAllocator());
            }
        }

        static rapidjson::SchemaDocument init_schema(const char* json) {
            rapidjson::Document doc;
            doc.Parse(json);
            if (doc.HasParseError()) throw rapidmessage::validation_error(doc);
            return rapidjson::SchemaDocument(doc);
        }

        static contact from_json(const char* buffer) {
            static const auto schema = init_schema(example_schema);

            contact c;
            c._document.Parse(buffer);
            if (c._document.HasParseError()) throw rapidmessage::validation_error(c._document);
            rapidjson::SchemaValidator validator(schema);
            if (!c._document.Accept(validator)) throw rapidmessage::validation_error(c._document, validator);

            return c;
        }

        rapidjson::Document& to_document() { return _document; }

        rapidjson::StringBuffer to_json() {
            rapidjson::StringBuffer buffer;
            rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
            _document.Accept(writer);
            return buffer;
        }

        template <typename T>
        void to_json(T& buffer) {
            rapidjson::Writer writer(buffer);
            _document.Accept(writer);
        }

    private:
        rapidjson::Document _document;
    };

}  // namespace typical_message

SCENARIO("typical_message") {
    GIVEN("A properly formatted json base_message") {
        auto contact = typical_message::contact::from_json(typical_message::example_data);

        REQUIRE(contact.id() == 3486);
        REQUIRE(contact.first_name() == "John");
        REQUIRE(contact.last_name() == "Doe");

        auto email_array = contact.email_addresses();
        REQUIRE(email_array[0] == "johndoe@gmail.com");
        REQUIRE(email_array[1] == "jdoe@business.com");

        auto phone_array = contact.phone_numbers();
        REQUIRE(phone_array[0] == "+1 (388) 385-2938");
        REQUIRE(phone_array[1] == "+1 (747) 472-3957");

        auto address = contact.addresses()[0];
        REQUIRE(address.line_1() == "274 W. 43rd St.");
        REQUIRE(address.line_2() == "#245");
        REQUIRE(address.city() == "Kansas City");
        REQUIRE(address.state() == "MO");
        REQUIRE(address.zip() == 64111);
    }

    GIVEN("An empty base_message for serialization") {
        auto contact = typical_message::contact();

        contact.id(3486);
        contact.first_name("John");
        contact.last_name("Doe");

        auto email_addresses = contact.email_addresses();
        email_addresses.insert("johndoe@gmail.com");
        email_addresses.insert("jdoe@business.com");

        auto phone_numbers = contact.phone_numbers();
        phone_numbers.insert("+1 (388) 385-2938");
        phone_numbers.insert("+1 (747) 472-3957");

        auto addresses = contact.addresses();
        auto address   = addresses.insert();
        address.line_1("274 W. 43rd St.");
        address.line_2("#245");
        address.city("Kansas City");
        address.state("MO");
        address.zip(64111);

        rapidjson::Document expected;
        expected.Parse(typical_message::example_data);
        REQUIRE(contact.to_document() == expected);
    }
}
