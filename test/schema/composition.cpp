
#include <catch2/catch.hpp>
#include <rapidmessage/schema.hpp>

namespace s = rapidmessage::schema;

#define COMPOSITION_TEST(comp_name, comp_key)                                                                          \
    GIVEN("an " #comp_name " composition") {                                                                           \
        auto doc = s::compile_document(s::comp_name(s::string(s::max_length(5)), s::number(s::minimum(0))));           \
        REQUIRE(doc[#comp_key][0]["type"] == "string");                                                                \
        REQUIRE(doc[#comp_key][0]["maxLength"] == 5);                                                                  \
        REQUIRE(doc[#comp_key][1]["type"] == "number");                                                                \
        REQUIRE(doc[#comp_key][1]["minimum"] == 0);                                                                    \
    }

#define CONDITIONAL_TEST(comp_name, comp_key)                                                                          \
    GIVEN("a not composition") {                                                                                       \
        auto doc = s::compile_document(s::comp_name(s::string(s::max_length(5))));                                     \
        REQUIRE(doc[#comp_key]["type"] == "string");                                                                   \
        REQUIRE(doc[#comp_key]["maxLength"] == 5);                                                                     \
    }

SCENARIO("schema: composition combinator") {
    COMPOSITION_TEST(all_of, allOf)
    COMPOSITION_TEST(any_of, anyOf)
    COMPOSITION_TEST(one_of, oneOf)

    CONDITIONAL_TEST(if_, if)
    CONDITIONAL_TEST(then_, then)
    CONDITIONAL_TEST(else_, else)
    CONDITIONAL_TEST(not_, not)
}
