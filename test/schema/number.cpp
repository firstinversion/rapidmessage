#include <catch2/catch.hpp>
#include <rapidmessage/schema.hpp>

namespace s = rapidmessage::schema;

#define VALUE_MODIFIER_TEST(modifier_class_name, modifier_field_name, modifier_value)                                  \
    GIVEN("a number combinator with " #modifier_class_name) {                                                          \
        auto doc =                                                                                                     \
            s::compile_document(s::object(s::property("x", s::number(s::modifier_class_name(modifier_value)))));       \
        REQUIRE(doc["properties"]["x"]["type"] == "number");                                                           \
        REQUIRE(doc["properties"]["x"][#modifier_field_name] == modifier_value);                                       \
    }

SCENARIO("schema: number combinator") {
    GIVEN("a default number combinator") {
        auto doc = s::compile_document(s::object(s::property("x", s::number{})));

        REQUIRE(doc["properties"]["x"].IsObject());
        REQUIRE(doc["properties"]["x"]["type"] == "number");
    }

    VALUE_MODIFIER_TEST(multiple_of, multipleOf, 3.0)
    VALUE_MODIFIER_TEST(minimum, minimum, 3.0)
    VALUE_MODIFIER_TEST(maximum, maximum, 3.0)
    VALUE_MODIFIER_TEST(exclusive_minimum, exclusiveMinimum, 3.0)
    VALUE_MODIFIER_TEST(exclusive_maximum, exclusiveMaximum, 3.0)

    GIVEN("a number combinator with draft 4 exclusive maximum") {
        auto doc =
            s::compile_document(s::object(s::property("x", s::number(s::maximum(3.0), s::exclusive_maximum(true)))));

        REQUIRE(doc["properties"]["x"].IsObject());
        REQUIRE(doc["properties"]["x"]["type"] == "number");
        REQUIRE(doc["properties"]["x"]["maximum"] == 3.0);
        REQUIRE(doc["properties"]["x"]["exclusiveMaximum"].GetBool());
    }
}

SCENARIO("schema: integer combinator") {
    GIVEN("a default integer combinator") {
        auto doc = s::compile_document(s::object(s::property("x", s::integer{})));

        REQUIRE(doc["properties"]["x"]["type"] == "integer");
    }

    VALUE_MODIFIER_TEST(multiple_of, multipleOf, 3)
    VALUE_MODIFIER_TEST(minimum, minimum, 3)
    VALUE_MODIFIER_TEST(maximum, maximum, 3)
    VALUE_MODIFIER_TEST(exclusive_minimum, exclusiveMinimum, 3)
    VALUE_MODIFIER_TEST(exclusive_maximum, exclusiveMaximum, 3)

    GIVEN("a number combinator with draft 4 exclusive maximum") {
        auto doc =
            s::compile_document(s::object(s::property("x", s::number(s::maximum(3), s::exclusive_maximum(true)))));

        REQUIRE(doc["properties"]["x"].IsObject());
        REQUIRE(doc["properties"]["x"]["type"] == "number");
        REQUIRE(doc["properties"]["x"]["maximum"] == 3);
        REQUIRE(doc["properties"]["x"]["exclusiveMaximum"].GetBool());
    }
}
