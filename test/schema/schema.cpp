#include <catch2/catch.hpp>
#include <rapidmessage/schema.hpp>

namespace s = rapidmessage::schema;

SCENARIO("schema: schema combinator") {
    GIVEN("a schema combinator") {
        auto doc = s::compile_document(s::object(s::schema("http://json-schema.org/schema#")));

        REQUIRE(doc["type"] == "object");
        REQUIRE(doc["$schema"] == "http://json-schema.org/schema#");
    }
}
