#include <catch2/catch.hpp>
#include <rapidmessage/schema.hpp>

namespace s = rapidmessage::schema;

SCENARIO("schema: string combinator") {
    GIVEN("a default string combinator") {
        auto doc = s::compile_document(s::object(s::property("_name", s::string{})));

        REQUIRE(doc["properties"]["_name"].IsObject());
        REQUIRE(doc["properties"]["_name"]["type"] == "string");
    }

    GIVEN("a string combinator with a min_length") {
        auto doc = s::compile_document(s::object(s::property("_name", s::string(s::min_length(2)))));

        REQUIRE(doc["properties"]["_name"].IsObject());
        REQUIRE(doc["properties"]["_name"]["type"] == "string");
        REQUIRE(doc["properties"]["_name"]["minLength"] == 2);
    }

    GIVEN("a string combinator with a max_length") {
        auto doc = s::compile_document(s::object(s::property("_name", s::string(s::max_length(10)))));

        REQUIRE(doc["properties"]["_name"].IsObject());
        REQUIRE(doc["properties"]["_name"]["type"] == "string");
        REQUIRE(doc["properties"]["_name"]["maxLength"] == 10);
    }

    GIVEN("a string combinator with a max_length") {
        auto doc = s::compile_document(s::object(s::property("_name", s::string(s::min_length(2), s::max_length(10)))));

        REQUIRE(doc["properties"]["_name"].IsObject());
        REQUIRE(doc["properties"]["_name"]["type"] == "string");
        REQUIRE(doc["properties"]["_name"]["minLength"] == 2);
        REQUIRE(doc["properties"]["_name"]["maxLength"] == 10);
    }

    GIVEN("a string combinator with a pattern") {
        auto doc = s::compile_document(
            s::object(s::property("phone", s::string(s::pattern(R"(^(\\([0-9]{3}\\))?[0-9]{3}-[0-9]{4}$)")))));

        REQUIRE(doc["properties"]["phone"]["type"] == "string");
        REQUIRE(doc["properties"]["phone"]["pattern"] == R"(^(\\([0-9]{3}\\))?[0-9]{3}-[0-9]{4}$)");
    }

    GIVEN("a string combinator with a format") {
        auto doc = s::compile_document(s::object(s::property("phone", s::string(s::format("date-time")))));

        REQUIRE(doc["properties"]["phone"]["type"] == "string");
        REQUIRE(doc["properties"]["phone"]["format"] == "date-time");
    }

    GIVEN("a string combinator with encoded data") {
        auto doc = s::compile_document(s::object(s::property(
            "profile_picture", s::string(s::content_encoding("base64"), s::content_media_type("image/png")))));

        REQUIRE(doc["properties"]["profile_picture"]["type"] == "string");
        REQUIRE(doc["properties"]["profile_picture"]["contentEncoding"] == "base64");
        REQUIRE(doc["properties"]["profile_picture"]["contentMediaType"] == "image/png");
    }
}
