#include <catch2/catch.hpp>
#include <rapidmessage/schema.hpp>

namespace s = rapidmessage::schema;

SCENARIO("schema: null combinator") {
    GIVEN("a null combinator") {
        auto doc = s::compile_document(s::object(s::property("nullval", s::null{})));

        REQUIRE(doc["properties"]["nullval"]["type"] == "null");
    }
}
