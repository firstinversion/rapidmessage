#include <catch2/catch.hpp>
#include <rapidmessage/schema.hpp>

namespace s = rapidmessage::schema;

SCENARIO("schema: array combinator") {
    GIVEN("an array combinator") {
        // clang-format off
        auto doc = s::compile_document(s::object(
                s::property("x", s::array(s::items(s::integer{})))
        ));
        // clang-format on

        REQUIRE(doc["properties"]["x"]["type"] == "array");
        REQUIRE(doc["properties"]["x"]["items"]["type"] == "integer");
    }

    GIVEN("a top level array combinator") {
        // clang-format off
        auto doc = s::compile_document(s::array(s::items(s::object(
                s::property("x", s::number{}),
                s::property("y", s::number{})
        ))));
        // clang-format on

        REQUIRE(doc["type"] == "array");
        REQUIRE(doc["items"]["type"] == "object");
        REQUIRE(doc["items"]["properties"]["x"]["type"] == "number");
        REQUIRE(doc["items"]["properties"]["y"]["type"] == "number");
    }

    GIVEN("an array with contains") {
        auto doc = s::compile_document(s::array(s::contains(s::number{})));

        REQUIRE(doc["type"] == "array");
        REQUIRE(doc["contains"]["type"] == "number");
    }

    GIVEN("an array that defines a tuple") {
        // clang-format off
        auto doc = s::compile_document(s::array(s::tuple(
                s::number{},
                s::integer{},
                s::string{}
        )));
        // clang-format on

        REQUIRE(doc["items"][0]["type"] == "number");
        REQUIRE(doc["items"][1]["type"] == "integer");
        REQUIRE(doc["items"][2]["type"] == "string");
    }

    GIVEN("an array tuple with additional items") {
        // clang-format off
        auto doc = s::compile_document(s::array(
                s::tuple(s::number{}, s::number{}, s::number{}),
                s::additional_items(s::string{})
        ));
        // clang-format on

        REQUIRE(doc["type"] == "array");
        REQUIRE(doc["items"][0]["type"] == "number");
        REQUIRE(doc["items"][1]["type"] == "number");
        REQUIRE(doc["items"][2]["type"] == "number");
        REQUIRE(doc["additionalItems"]["type"] == "string");
    }

    GIVEN("an array with min_items") {
        auto doc = s::compile_document(s::array(s::min_items(2)));
        REQUIRE(doc["minItems"] == 2);
    }

    GIVEN("an array with max_items") {
        auto doc = s::compile_document(s::array(s::max_items(3)));
        REQUIRE(doc["maxItems"] == 3);
    }

    GIVEN("an array with uniqueness") {
        auto doc = s::compile_document(s::array(s::unique_items(true)));
        REQUIRE(doc["uniqueItems"].GetBool());
    }
}
