#include <catch2/catch.hpp>
#include <rapidmessage/schema.hpp>

namespace s = rapidmessage::schema;

SCENARIO("schema: boolean combinator") {
    GIVEN("a boolean combinator") {
        auto doc = s::compile_document(s::object(s::property("alive", s::boolean{})));

        REQUIRE(doc["properties"]["alive"]["type"] == "boolean");
    }
}
