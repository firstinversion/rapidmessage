#include <catch2/catch.hpp>
#include <rapidmessage/schema.hpp>

namespace s = rapidmessage::schema;

#define STRING_ANNOTATION_TEST(annotation, annotation_key, on_combinator)                                              \
    GIVEN("a " #annotation " annotation on a " #on_combinator " combinator") {                                         \
        auto doc = s::compile_document(s::object(s::property("value", s::on_combinator(s::annotation("value")))));     \
        REQUIRE(doc["properties"]["value"][#annotation_key] == "value");                                               \
    }

#define VALUE_ANNOTATION_TEST(annotation, annotation_key, annotation_value_type, annotation_value, on_combinator)      \
    GIVEN("a " #annotation " annotation on a " #on_combinator " combinator") {                                         \
        auto doc = s::compile_document(s::object(                                                                      \
            s::property("value", s::on_combinator(s::annotation<annotation_value_type>(annotation_value)))));          \
        REQUIRE(doc["properties"]["value"][#annotation_key] == annotation_value);                                      \
    }

#define VALUES_ANNOTATION_TEST(                                                                                        \
    annotation, annotation_key, annotation_value_type, annotation_value_1, annotation_value_2, on_combinator)          \
    GIVEN("a " #annotation " annotation on a " #on_combinator " combinator") {                                         \
        auto doc = s::compile_document(                                                                                \
            s::object(s::property("value1",                                                                            \
                                  s::on_combinator(s::annotation<annotation_value_type, annotation_value_type>(        \
                                      annotation_value_1, annotation_value_2)))));                                     \
        REQUIRE(doc["properties"]["value1"][#annotation_key][0] == annotation_value_1);                                \
        REQUIRE(doc["properties"]["value1"][#annotation_key][1] == annotation_value_2);                                \
    }

#define EXAMPLE_ANNOTATION_TEST(on_combinator)                                                                         \
    GIVEN("an example annotation on a " #on_combinator " combinator") {                                                \
        auto doc = s::compile_document(                                                                                \
            s::object(s::property("name", s::on_combinator(s::examples("John Doe", "Jane Doe")))));                    \
        REQUIRE(doc["properties"]["name"]["examples"][0] == "John Doe");                                               \
        REQUIRE(doc["properties"]["name"]["examples"][1] == "Jane Doe");                                               \
    }

SCENARIO("schema: annotations") {
    STRING_ANNOTATION_TEST(title, title, null)
    STRING_ANNOTATION_TEST(title, title, boolean)
    STRING_ANNOTATION_TEST(title, title, number)
    STRING_ANNOTATION_TEST(title, title, integer)
    STRING_ANNOTATION_TEST(title, title, string)
    STRING_ANNOTATION_TEST(title, title, array)
    STRING_ANNOTATION_TEST(title, title, object)

    STRING_ANNOTATION_TEST(description, description, null)
    STRING_ANNOTATION_TEST(description, description, boolean)
    STRING_ANNOTATION_TEST(description, description, number)
    STRING_ANNOTATION_TEST(description, description, integer)
    STRING_ANNOTATION_TEST(description, description, string)
    STRING_ANNOTATION_TEST(description, description, array)
    STRING_ANNOTATION_TEST(description, description, object)

    VALUE_ANNOTATION_TEST(default_, default, bool, true, boolean)
    VALUE_ANNOTATION_TEST(default_, default, float, 0.0, number)
    VALUE_ANNOTATION_TEST(default_, default, int32_t, 0, integer)
    VALUE_ANNOTATION_TEST(default_, default, const char*, static_cast<const char*>("john doe"), string)

    EXAMPLE_ANNOTATION_TEST(null)
    EXAMPLE_ANNOTATION_TEST(boolean)
    EXAMPLE_ANNOTATION_TEST(number)
    EXAMPLE_ANNOTATION_TEST(integer)
    EXAMPLE_ANNOTATION_TEST(string)
    EXAMPLE_ANNOTATION_TEST(array)
    EXAMPLE_ANNOTATION_TEST(object)

    VALUES_ANNOTATION_TEST(enum_, enum, bool, true, false, boolean)
    VALUES_ANNOTATION_TEST(enum_, enum, float, 0.0, 1.0, number)
    VALUES_ANNOTATION_TEST(enum_, enum, int32_t, 0, 1, integer)
    VALUES_ANNOTATION_TEST(enum_, enum, const char*, "john", "jane", string)

    VALUE_ANNOTATION_TEST(const_, const, bool, true, boolean)
    VALUE_ANNOTATION_TEST(const_, const, float, 0.0, number)
    VALUE_ANNOTATION_TEST(const_, const, int32_t, 0, integer)
    VALUE_ANNOTATION_TEST(const_, const, const char*, "john doe", string)
}
