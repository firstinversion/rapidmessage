#include <catch2/catch.hpp>
#include <rapidmessage/schema.hpp>

namespace s = rapidmessage::schema;

SCENARIO("schema: object combinator") {
    GIVEN("an object combinator") {
        auto doc = s::compile_document(s::object(s::property("x", s::number{}), s::property("y", s::number{})));

        REQUIRE(doc["properties"]["x"]["type"] == "number");
        REQUIRE(doc["properties"]["y"]["type"] == "number");
    }

    GIVEN("an object combinator with no additional properties") {
        auto doc = s::compile_document(
            s::object(s::property("x", s::number{}), s::property("y", s::number{}), s::additional_properties(false)));

        REQUIRE(doc["properties"]["x"]["type"] == "number");
        REQUIRE(doc["properties"]["y"]["type"] == "number");
        REQUIRE(!doc["additionalProperties"].GetBool());
    }

    GIVEN("an object combinator with additional properties of a schema") {
        auto doc = s::compile_document(s::object(
            s::property("x", s::number{}), s::property("y", s::number{}), s::additional_properties(s::string{})));

        REQUIRE(doc["properties"]["x"]["type"] == "number");
        REQUIRE(doc["properties"]["y"]["type"] == "number");
        REQUIRE(doc["additionalProperties"]["type"] == "string");
    }

    GIVEN("an object combinator with required properties") {
        auto doc = s::compile_document(
            s::object(s::property("x", s::number{}), s::property("y", s::number{}), s::required("x", "y")));

        REQUIRE(doc["properties"]["x"]["type"] == "number");
        REQUIRE(doc["properties"]["y"]["type"] == "number");
        REQUIRE(doc["required"][0] == "x");
        REQUIRE(doc["required"][1] == "y");
    }

    GIVEN("an object combinator with required properties defined by the property") {
        auto doc = s::compile_document(s::object(s::property("x", s::number{}, s::required{}),
                                                 s::property("y", s::number{}, s::required{}),
                                                 s::property("z", s::number{}),
                                                 s::required("z")));

        REQUIRE(doc["properties"]["x"]["type"] == "number");
        REQUIRE(doc["properties"]["y"]["type"] == "number");
        REQUIRE(doc["properties"]["z"]["type"] == "number");
        REQUIRE(doc["required"][0] == "x");
        REQUIRE(doc["required"][1] == "y");
        REQUIRE(doc["required"][2] == "z");
    }

    GIVEN("an object combinator with property_names") {
        auto doc = s::compile_document(s::object(s::property_names("^[A-Za-z_][A-Za-z0-9_]*$")));

        REQUIRE(doc["propertyNames"]["pattern"] == "^[A-Za-z_][A-Za-z0-9_]*$");
    }

    GIVEN("an object combinator with size limits") {
        auto doc = s::compile_document(s::object(s::min_properties(2), s::max_properties(3)));

        REQUIRE(doc["minProperties"] == 2);
        REQUIRE(doc["maxProperties"] == 3);
    }

    GIVEN("an object with field dependency") {
        auto doc = s::compile_document(s::object(s::property("x", s::number{}, s::depends("y")),
                                                 s::property("y", s::number{}, s::depends("x")),
                                                 s::property("z", s::number{}, s::depends("x", "y"))));

        REQUIRE(doc["properties"]["x"]["type"] == "number");
        REQUIRE(doc["properties"]["y"]["type"] == "number");
        REQUIRE(doc["properties"]["z"]["type"] == "number");
        REQUIRE(doc["dependencies"]["x"][0] == "y");
        REQUIRE(doc["dependencies"]["y"][0] == "x");
        REQUIRE(doc["dependencies"]["z"][0] == "x");
        REQUIRE(doc["dependencies"]["z"][1] == "y");
    }

    GIVEN("an object with schema dependency") {
        auto doc = s::compile_document(
            s::object(s::property("credit_card",
                                  s::number{},
                                  s::depends_schema(s::object(s::property("billing_address", s::string{}),
                                                              s::required("billing_address"))))));

        REQUIRE(doc["properties"]["credit_card"]["type"] == "number");
        REQUIRE(doc["dependencies"]["credit_card"]["properties"]["billing_address"]["type"] == "string");
        REQUIRE(doc["dependencies"]["credit_card"]["required"][0] == "billing_address");
    }

    GIVEN("an object with pattern properties") {
        auto doc = s::compile_document(s::object(s::property("builtin", s::number{}),
                                                 s::pattern_property("^S_", s::string{}),
                                                 s::pattern_property("^I_", s::integer{})));

        REQUIRE(doc["properties"]["builtin"]["type"] == "number");
        REQUIRE(doc["patternProperties"]["^S_"]["type"] == "string");
        REQUIRE(doc["patternProperties"]["^I_"]["type"] == "integer");
    }
}
