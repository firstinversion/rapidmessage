#include <catch2/catch.hpp>

#include <rapidmessage/array.hpp>

namespace test::arrays {

    const char* data_root_bools = R"([true, false])";
    const char* data_sub_bools  = R"({"bools": [true, false]})";

    const char* data_root_nums = R"([234.25, 35.236])";
    const char* data_sub_nums  = R"({"nums": [234.25, 35.236]})";

    const char* data_root_ints = R"([234, 35])";
    const char* data_sub_ints  = R"({"ints": [234, 35]})";

    const char* data_root_strs = R"(["john", "jane"])";
    const char* data_sub_strs  = R"({"strs": ["john", "jane"]})";

    const char* data_root_objs = R"([{"x": 1, "y", 1}, {"x": 2, "y": 2}])";
    const char* data_sub_objs  = R"({"objs": [{"x": 1, "y", 1}, {"x": 2, "y": 2}]})";

    rapidjson::Document parse(const char* data) {
        rapidjson::Document document;
        document.Parse(data);
        return document;
    }

}  // namespace test::arrays

TEST_CASE("extract bools from root array") {
    auto doc = test::arrays::parse(test::arrays::data_root_bools);
    auto ary = rapidmessage::array<bool>(doc, doc.GetAllocator());
    REQUIRE(ary[0]);
    REQUIRE(!ary[1]);
}

TEST_CASE("extract bools from sub array") {
    auto doc = test::arrays::parse(test::arrays::data_sub_bools);
    auto ary = rapidmessage::array<bool>(doc["bools"], doc.GetAllocator());
    REQUIRE(ary[0]);
    REQUIRE(!ary[1]);
}

TEST_CASE("extract floats from sub array") {
    auto doc = test::arrays::parse(test::arrays::data_sub_nums);
    auto ary = rapidmessage::array<float>(doc["nums"], doc.GetAllocator());
    REQUIRE(ary[0] == 234.25f);
    REQUIRE(ary[1] == 35.236f);
}

TEST_CASE("extract floats from root array") {
    auto doc = test::arrays::parse(test::arrays::data_root_nums);
    auto ary = rapidmessage::array<float>(doc, doc.GetAllocator());
    REQUIRE(ary[0] == 234.25f);
    REQUIRE(ary[1] == 35.236f);
}

TEST_CASE("extract doubles from sub array") {
    auto doc = test::arrays::parse(test::arrays::data_sub_nums);
    auto ary = rapidmessage::array<double>(doc["nums"], doc.GetAllocator());
    REQUIRE(ary[0] == 234.25);
    REQUIRE(ary[1] == 35.236);
}

TEST_CASE("extract doubles from root array") {
    auto doc = test::arrays::parse(test::arrays::data_root_nums);
    auto ary = rapidmessage::array<double>(doc, doc.GetAllocator());
    REQUIRE(ary[0] == 234.25);
    REQUIRE(ary[1] == 35.236);
}

TEST_CASE("extract int32_t from sub array") {
    auto doc = test::arrays::parse(test::arrays::data_sub_ints);
    auto ary = rapidmessage::array<int32_t>(doc["ints"], doc.GetAllocator());
    REQUIRE(ary[0] == 234);
    REQUIRE(ary[1] == 35);
}

TEST_CASE("extract int32_t from root array") {
    auto doc = test::arrays::parse(test::arrays::data_root_ints);
    auto ary = rapidmessage::array<int32_t>(doc, doc.GetAllocator());
    REQUIRE(ary[0] == 234);
    REQUIRE(ary[1] == 35);
}

TEST_CASE("extract uint32_t from sub array") {
    auto doc = test::arrays::parse(test::arrays::data_sub_ints);
    auto ary = rapidmessage::array<uint32_t>(doc["ints"], doc.GetAllocator());
    REQUIRE(ary[0] == 234);
    REQUIRE(ary[1] == 35);
}

TEST_CASE("extract uint32_t from root array") {
    auto doc = test::arrays::parse(test::arrays::data_root_ints);
    auto ary = rapidmessage::array<uint32_t>(doc, doc.GetAllocator());
    REQUIRE(ary[0] == 234);
    REQUIRE(ary[1] == 35);
}

TEST_CASE("extract int64_t from sub array") {
    auto doc = test::arrays::parse(test::arrays::data_sub_ints);
    auto ary = rapidmessage::array<int64_t>(doc["ints"], doc.GetAllocator());
    REQUIRE(ary[0] == 234);
    REQUIRE(ary[1] == 35);
}

TEST_CASE("extract int64_t from root array") {
    auto doc = test::arrays::parse(test::arrays::data_root_ints);
    auto ary = rapidmessage::array<int64_t>(doc, doc.GetAllocator());
    REQUIRE(ary[0] == 234);
    REQUIRE(ary[1] == 35);
}

TEST_CASE("extract uint64_t from sub array") {
    auto doc = test::arrays::parse(test::arrays::data_sub_ints);
    auto ary = rapidmessage::array<uint64_t>(doc["ints"], doc.GetAllocator());
    REQUIRE(ary[0] == 234);
    REQUIRE(ary[1] == 35);
}

TEST_CASE("extract uint64_t from root array") {
    auto doc = test::arrays::parse(test::arrays::data_root_ints);
    auto ary = rapidmessage::array<uint64_t>(doc, doc.GetAllocator());
    REQUIRE(ary[0] == 234);
    REQUIRE(ary[1] == 35);
}

TEST_CASE("extract string from sub array") {
    auto doc = test::arrays::parse(test::arrays::data_sub_strs);
    auto ary = rapidmessage::array<std::string_view>(doc["strs"], doc.GetAllocator());
    REQUIRE(ary[0] == "john");
    REQUIRE(ary[1] == "jane");
}

TEST_CASE("extract string from root array") {
    auto doc = test::arrays::parse(test::arrays::data_root_strs);
    auto ary = rapidmessage::array<std::string_view>(doc, doc.GetAllocator());
    REQUIRE(ary[0] == "john");
    REQUIRE(ary[1] == "jane");
}

TEST_CASE("size of array") {
    auto doc = test::arrays::parse(test::arrays::data_root_ints);
    auto ary = rapidmessage::array<int32_t>(doc, doc.GetAllocator());
    REQUIRE(ary.size() == 2);
}

TEST_CASE("non-empty of array") {
    auto doc = test::arrays::parse(test::arrays::data_root_ints);
    auto ary = rapidmessage::array<int32_t>(doc, doc.GetAllocator());
    REQUIRE(!ary.empty());
}

TEST_CASE("empty of array") {
    auto doc = test::arrays::parse(R"([])");
    auto ary = rapidmessage::array<int32_t>(doc, doc.GetAllocator());
    REQUIRE(ary.empty());
}
