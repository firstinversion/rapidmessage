#include <catch2/catch.hpp>

#include <rapidjson/document.h>
#include <rapidmessage/proto_rapidmessage.hpp>

// clang-format off
MESSAGE_CLASS(json_point,
        (double, x)
                (double, y)
                (double, z))
// clang-format on
