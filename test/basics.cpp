#include <catch2/catch.hpp>

#include <rapidmessage/proto_rapidmessage.hpp>

// clang-format off
MESSAGE_CLASS(basic_person,
        (std::string, first_name)
                (std::string, last_name)
                (uint32_t, age))
// clang-format on

TEST_CASE("person") {
    basic_person p;
    p.first_name("John");
    p.last_name("Doe");
    p.age(34);

    REQUIRE(p.first_name() == "John");
    REQUIRE(p.last_name() == "Doe");
    REQUIRE(p.age() == 34);
}

// clang-format off
MESSAGE_CLASS(basic_address,
        (std::string, line_1)
                (std::string, line_2)
                (std::string, city)
                (std::string, state)
                (uint32_t, zip))
// clang-format on

TEST_CASE("address") {
    basic_address a;
    a.line_1("111 W. 23rd St");
    a.line_2("#345");
    a.city("Kansas City");
    a.state("MO");
    a.zip(64111);

    REQUIRE(a.line_1() == "111 W. 23rd St");
    REQUIRE(a.line_2() == "#345");
    REQUIRE(a.city() == "Kansas City");
    REQUIRE(a.state() == "MO");
    REQUIRE(a.zip() == 64111);
}
