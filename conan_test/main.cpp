#include <rapidmessage/object.hpp>
#include <rapidmessage/schema.hpp>

const char* data_array_field = R"({
    "points": [
        {"x": 34, "y": 47},
        {"x": 542, "y": 234}
    ]
})";

RAPID_MESSAGE(geometry) {
public:
    RAPID_OBJECT(point) {
    public:
        RAPID_OBJECT_CONSTRUCTORS(point);

        RAPID_PROP_BYVAL(int32_t, x);
        RAPID_PROP_BYVAL(int32_t, y);
    };

    RAPID_MESSAGE_CONSTRUCTORS(geometry);

    RAPID_PROP_BYREF(rapidmessage::array<geometry::point>, points);

    const rapidjson::SchemaDocument& schema() const override {
        namespace s = rapidmessage::schema;
        static const auto schema_document = s::compile(s::object(
                s::property("points", s::array(s::items(s::object(
                        s::property("x", s::integer{}),
                        s::property("y", s::integer{})
                ))))
        ));
        return schema_document;
    }
};

int main() {
    geometry g(data_array_field);
    return 0;
}
