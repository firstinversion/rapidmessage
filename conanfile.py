from conans import ConanFile, CMake, tools


class RapidmessageConan(ConanFile):
    name = "rapidmessage"
    version = "0.5.12"
    license = "MIT"
    author = "Andrew Rademacher <andrewrademacher@icloud.com>"
    url = "https://github.com/firstinversion/rapidmessage"
    description = "Opinionated tooling for defining type safe messages on top of RapidJSON."
    topics = "json"
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    exports_sources = "*"
    requires = ("boost/1.71.0@conan/stable",
                "rapidjson/1.1.0@bincrafters/stable",
                "fmt/6.0.0@bincrafters/stable")
    build_requires = "Catch2/2.10.2@catchorg/stable"

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
        if tools.get_env("CONAN_RUN_TEST", False):
            cmake.test()


    def package_id(self):
        self.info.header_only()

    def package(self):
        cmake = CMake(self)
        cmake.install()
