#pragma once

#include <rapidmessage/schema/annotations.hpp>
#include <rapidmessage/schema/array.hpp>
#include <rapidmessage/schema/boolean.hpp>
#include <rapidmessage/schema/compiler.hpp>
#include <rapidmessage/schema/composition.hpp>
#include <rapidmessage/schema/null.hpp>
#include <rapidmessage/schema/number.hpp>
#include <rapidmessage/schema/object.hpp>
#include <rapidmessage/schema/schema.hpp>
#include <rapidmessage/schema/string.hpp>
