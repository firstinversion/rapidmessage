#pragma once

#include <boost/hana/for_each.hpp>
#include <boost/hana/tuple.hpp>

namespace rapidmessage::schema {

    /**
     * Concept: Component
     */
    template <typename... Modifiers>
    class number {
    public:
        explicit number(Modifiers... modifiers)
            : _modifiers(modifiers...) {}

        void apply(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator) const {
            value.SetObject();
            value.AddMember("type", "number", allocator);
            boost::hana::for_each(_modifiers, [&](auto& modifier) { modifier.apply(value, allocator); });
        }

    private:
        boost::hana::tuple<Modifiers...> _modifiers;
    };

    /**
     * Concept: component
     */
    template <typename... Modifiers>
    class integer {
    public:
        explicit integer(Modifiers... modifiers)
            : _modifiers(modifiers...) {}

        void apply(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator) const {
            value.SetObject();
            value.AddMember("type", "integer", allocator);
            boost::hana::for_each(_modifiers, [&](auto& modifier) { modifier.apply(value, allocator); });
        }

    private:
        boost::hana::tuple<Modifiers...> _modifiers;
    };

// Generic _value modifier
#define GENERIC_VALUE_MODIFIER(modifier_class_name, modifier_field_name)                                               \
    template <typename ValueType>                                                                                      \
    class modifier_class_name {                                                                                        \
    public:                                                                                                            \
        explicit modifier_class_name(ValueType value)                                                                  \
            : _value(value) {}                                                                                         \
                                                                                                                       \
        void apply(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator) const {                     \
            value.AddMember(#modifier_field_name, rapidjson::Value(_value), allocator);                                \
        }                                                                                                              \
                                                                                                                       \
    private:                                                                                                           \
        ValueType _value;                                                                                              \
    }

    GENERIC_VALUE_MODIFIER(multiple_of, multipleOf);
    GENERIC_VALUE_MODIFIER(minimum, minimum);
    GENERIC_VALUE_MODIFIER(maximum, maximum);
    GENERIC_VALUE_MODIFIER(exclusive_minimum, exclusiveMinimum);
    GENERIC_VALUE_MODIFIER(exclusive_maximum, exclusiveMaximum);

#undef GENERIC_VALUE_MODIFIER

}  // namespace rapidmessage::schema
