#pragma once

#include <boost/hana/for_each.hpp>
#include <boost/hana/tuple.hpp>
#include <rapidjson/document.h>

namespace rapidmessage::schema {

#define STRING_ANNOTATION(annotation_name, annotation_key)                                                             \
    class annotation_name {                                                                                            \
    public:                                                                                                            \
        explicit annotation_name(const char* message)                                                                  \
            : _message(message) {}                                                                                     \
                                                                                                                       \
        void apply(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator) const {                     \
            value.AddMember(#annotation_key, rapidjson::Value().SetString(_message, allocator), allocator);            \
        }                                                                                                              \
                                                                                                                       \
    private:                                                                                                           \
        const char* _message;                                                                                          \
    }

    STRING_ANNOTATION(title, title);

    STRING_ANNOTATION(description, description);

#undef STRING_ANNOTATION

    /**
     * Concept: modifier
     */
    template <typename ValueType>
    class default_ {
    public:
        explicit default_(ValueType&& message)
            : _message(std::forward<ValueType>(message)) {}

        void apply(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator) const {
            if constexpr (std::is_same<const char*, typename std::decay<ValueType>::type>::value) {
                value.AddMember("default", rapidjson::Value().SetString(_message, allocator), allocator);
            } else {
                value.AddMember("default", rapidjson::Value(_message), allocator);
            }
        }

    private:
        ValueType _message;
    };

    /**
     * Concept: modifier
     */
    template <typename... Examples>
    class examples {
    public:
        explicit examples(Examples... examples)
            : _examples(examples...) {}

        void apply(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator) const {
            rapidjson::Value example_collection{rapidjson::kArrayType};
            boost::hana::for_each(_examples, [&](auto& ex) {
                using ValueT = std::decay_t<decltype(ex)>;
                if constexpr (std::is_same_v<const char*, ValueT>) {
                    example_collection.PushBack(rapidjson::Value{}.SetString(ex, allocator), allocator);
                } else {
                    example_collection.PushBack(rapidjson::Value(ex), allocator);
                }
            });
            value.AddMember("examples", example_collection, allocator);
        }

    private:
        boost::hana::tuple<Examples...> _examples;
    };

    /**
     * Concept: modifier
     */
    template <typename... ValueTypes>
    class enum_ {
    public:
        explicit enum_(ValueTypes... values)
            : _values(values...) {}

        void apply(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator) const {
            rapidjson::Value enum_values(rapidjson::kArrayType);
            boost::hana::for_each(_values, [&](auto& value) {
                using ValueT = decltype(value);
                if constexpr (std::is_same_v<const char*, std::decay_t<ValueT>>) {
                    enum_values.PushBack(rapidjson::Value().SetString(value, allocator), allocator);
                } else {
                    enum_values.PushBack(rapidjson::Value(value), allocator);
                }
            });
            value.AddMember("enum", enum_values, allocator);
        }

    private:
        boost::hana::tuple<ValueTypes...> _values;
    };

    /**
     * Concept: modifier
     */
    template <typename ValueType>
    class const_ {
    public:
        explicit const_(ValueType value)
            : _value(value) {}

        void apply(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator) const {
            if constexpr (std::is_same_v<const char*, ValueType>) {
                value.AddMember("const", rapidjson::Value().SetString(_value, allocator), allocator);
            } else {
                value.AddMember("const", rapidjson::Value(_value), allocator);
            }
        }

    private:
        ValueType _value;
    };

}  // namespace rapidmessage::schema
