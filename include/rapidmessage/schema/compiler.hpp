#pragma once

#include <rapidjson/schema.h>

namespace rapidmessage::schema {

    template <typename Component>
    inline rapidjson::Document compile_document(Component&& root) {
        rapidjson::Document document;
        root.apply(document, document.GetAllocator());
        return document;
    }

    template <typename Component>
    inline rapidjson::SchemaDocument compile(Component&& root) {
        return rapidjson::SchemaDocument(compile_document(std::forward<Component>(root)));
    }

}  // namespace rapidmessage::schema
