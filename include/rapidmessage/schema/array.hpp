#pragma once

#include <boost/hana/for_each.hpp>
#include <boost/hana/tuple.hpp>
#include <rapidjson/document.h>

namespace rapidmessage::schema {

    /**
     * Concept: component
     * @tparam Modifiers
     */
    template <typename... Modifiers>
    class array {
    public:
        explicit array(Modifiers... modifier)
            : _modifier(modifier...) {}

        void apply(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator) const {
            value.SetObject();
            value.AddMember("type", "array", allocator);
            boost::hana::for_each(_modifier, [&](auto& item) { item.apply(value, allocator); });
        }

    private:
        boost::hana::tuple<Modifiers...> _modifier;
    };

    /**
     * Concept: array modifier
     */
    template <typename Item>
    class items {
    public:
        explicit items(Item item)
            : _item(item) {}

        void apply(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator) const {
            rapidjson::Value items(rapidjson::kObjectType);
            _item.apply(items, allocator);
            value.AddMember("items", items, allocator);
        }

    private:
        Item _item;
    };

    /**
     * Concept: array modifier
     */
    template <typename Item>
    class contains {
    public:
        explicit contains(Item item)
            : _item(item) {}

        void apply(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator) const {
            rapidjson::Value contains(rapidjson::kObjectType);
            _item.apply(contains, allocator);
            value.AddMember("contains", contains, allocator);
        }

    private:
        Item _item;
    };

    /**
     * Concept: array modifier
     */
    template <typename... Items>
    class tuple {
    public:
        explicit tuple(Items... items)
            : _items(items...) {}

        void apply(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator) const {
            rapidjson::Value items(rapidjson::kArrayType);
            boost::hana::for_each(_items, [&](auto& item) {
                rapidjson::Value i;
                item.apply(i, allocator);
                items.PushBack(i, allocator);
            });
            value.AddMember("items", items, allocator);
        }

    private:
        boost::hana::tuple<Items...> _items;
    };

    /**
     * Concept: array modifier
     */
    template <typename Item>
    class additional_items {
    public:
        explicit additional_items(Item item)
            : _item(item) {}

        void apply(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator) const {
            rapidjson::Value item;
            _item.apply(item, allocator);
            value.AddMember("additionalItems", item, allocator);
        }

    private:
        Item _item;
    };

    /**
     * Concept: array modifier
     */
    class min_items {
    public:
        explicit min_items(size_t min)
            : _min(min) {}

        void apply(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator) const {
            value.AddMember("minItems", rapidjson::Value().SetInt64(_min), allocator);
        }

    private:
        size_t _min;
    };

    /**
     * Concept: array modifier
     */
    class max_items {
    public:
        explicit max_items(size_t max)
            : _max(max) {}

        void apply(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator) const {
            value.AddMember("maxItems", rapidjson::Value().SetInt64(_max), allocator);
        }

    private:
        size_t _max;
    };

    /**
     * Concept: array modifier
     */
    class unique_items {
    public:
        explicit unique_items(bool unique)
            : _unique(unique) {}

        void apply(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator) const {
            value.AddMember("uniqueItems", rapidjson::Value().SetBool(_unique), allocator);
        }

    private:
        bool _unique;
    };

}  // namespace rapidmessage::schema
