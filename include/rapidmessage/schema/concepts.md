# Component

| Expression | Type | Semantics |
| ---------- | ---- | --------- |
| C::apply(rapidjson::Value& _value, rapidjson::Document& allocator) const | | Make approperate modifiactions to the JSON document representing the json schema. |

Must support generic modifiers.

# NamedComponent

std::pair<const char*, Component>

# Modifier

The same as a component, but it cannot make a call to `_value.Set...`. We make the assumption that modifiers are called by components which have already initialized the JSON _value.

# Object Modifier

Same as modifier except signature is `C::apply(rapidjson::Value& object, rapidjson::Value& properties, rapidjson::Document::AllocatorType& allocator) const;`.

# Array Modifier

Modifier, that operates on parent level array.
