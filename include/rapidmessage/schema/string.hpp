#pragma once

#include <boost/hana/for_each.hpp>
#include <boost/hana/tuple.hpp>

namespace rapidmessage::schema {

    /**
     * Concept: component
     */
    template <typename... Modifiers>
    class string {
    public:
        explicit string(Modifiers... modifiers)
            : _modifiers(modifiers...) {}

        void apply(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator) const {
            value.SetObject();
            value.AddMember("type", "string", allocator);
            boost::hana::for_each(_modifiers, [&](auto& modifier) { modifier.apply(value, allocator); });
        }

    private:
        boost::hana::tuple<Modifiers...> _modifiers;
    };

#define STRING_INT_MODIFIER(modifier_name, modifier_key, param_name)                                                   \
    class modifier_name {                                                                                              \
    public:                                                                                                            \
        explicit modifier_name(int64_t param_name)                                                                     \
            : _##param_name(param_name) {}                                                                             \
                                                                                                                       \
        void apply(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator) const {                     \
            value.AddMember(#modifier_key, rapidjson::Value(_##param_name), allocator);                                \
        }                                                                                                              \
                                                                                                                       \
    private:                                                                                                           \
        int64_t _##param_name;                                                                                         \
    }

#define STRING_STRING_MODIFIER(modifier_name, modifier_key, param_name)                                                \
    class modifier_name {                                                                                              \
    public:                                                                                                            \
        explicit modifier_name(const char* param_name)                                                                 \
            : _##param_name(param_name) {}                                                                             \
                                                                                                                       \
        void apply(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator) const {                     \
            value.AddMember(#modifier_key, rapidjson::Value().SetString(_##param_name, allocator), allocator);         \
        }                                                                                                              \
                                                                                                                       \
    private:                                                                                                           \
        const char* _##param_name;                                                                                     \
    }

    STRING_INT_MODIFIER(min_length, minLength, length);

    STRING_INT_MODIFIER(max_length, maxLength, length);

    STRING_STRING_MODIFIER(pattern, pattern, regex_pattern);

    STRING_STRING_MODIFIER(format, format, format_string);

    STRING_STRING_MODIFIER(content_encoding, contentEncoding, encoding);

    STRING_STRING_MODIFIER(content_media_type, contentMediaType, media_type);

#undef STRING_INT_MODIFIER
#undef STRING_STRING_MODIFIER

}  // namespace rapidmessage::schema
