#pragma once

#include <rapidjson/document.h>

namespace rapidmessage::schema {

    /**
     * Concept: component
     */
    template <typename... Modifiers>
    class boolean {
    public:
        explicit boolean(Modifiers... modifiers)
            : _modifiers(modifiers...) {}

        void apply(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator) const {
            value.SetObject();
            value.AddMember("type", "boolean", allocator);
            boost::hana::for_each(_modifiers, [&](auto& mod) { mod.apply(value, allocator); });
        }

    private:
        boost::hana::tuple<Modifiers...> _modifiers;
    };

}  // namespace rapidmessage::schema
