#pragma once

#include <rapidjson/document.h>

namespace rapidmessage::schema {

    /**
     * Concept: component
     */
    class schema {
    public:
        explicit schema(const char* uri)
            : _uri(uri) {}

        void apply(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator) const {
            assert(value.IsObject());
            value.AddMember("$schema", rapidjson::Value().SetString(_uri, allocator), allocator);
        }

    private:
        const char* _uri;
    };

}  // namespace rapidmessage::schema
