#pragma once

#include <boost/hana.hpp>
#include <rapidjson/document.h>

namespace rapidmessage::schema {

    struct object_components {
        rapidjson::Value& object;
        rapidjson::Value& properties;
        rapidjson::Value& pattern_properties;
        rapidjson::Value& dependencies;
    };

    class object_modifier {};

    /**
     * Concept: Component
     */
    template <typename... NamedComponents>
    class object {
    public:
        explicit object(NamedComponents... components)
            : _modifiers(components...) {}

        void apply(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator) const {
            value.SetObject();
            value.AddMember("type", "object", allocator);

            rapidjson::Value properties(rapidjson::kObjectType);
            rapidjson::Value pattern_properties(rapidjson::kObjectType);
            rapidjson::Value dependencies(rapidjson::kObjectType);

            boost::hana::for_each(_modifiers, [&](auto& field) {
                if constexpr (std::is_base_of_v<object_modifier, std::decay_t<decltype(field)>>) {
                    field.apply(object_components{value, properties, pattern_properties, dependencies}, allocator);
                } else {
                    field.apply(value, allocator);
                }
            });

            value.AddMember("properties", properties, allocator);
            value.AddMember("patternProperties", pattern_properties, allocator);
            value.AddMember("dependencies", dependencies, allocator);
        }

    private:
        boost::hana::tuple<NamedComponents...> _modifiers;
    };

    /**
     * Concept: field modifier
     * Do not use with depends_schema
     */
    template <typename... StringLike>
    class depends {
    public:
        explicit depends(StringLike... depends)
            : _depends(depends...) {}

        void apply(object_components parent, const char* field_name,
                   rapidjson::Document::AllocatorType& allocator) const {
            rapidjson::Value field_depends(rapidjson::kArrayType);
            boost::hana::for_each(_depends, [&](auto& dep) {
                field_depends.PushBack(rapidjson::Value().SetString(dep, allocator), allocator);
            });
            parent.dependencies.AddMember(
                rapidjson::Value().SetString(field_name, allocator), field_depends, allocator);
        }

    private:
        boost::hana::tuple<StringLike...> _depends;
    };

    /**
     * Concept: field modifier
     * Do not use with depends
     */
    template <typename... NamedComponents>
    class depends_schema {
    public:
        explicit depends_schema(object<NamedComponents...>&& object)
            : _object(std::forward<rapidmessage::schema::object<NamedComponents...>>(object)) {}

        void apply(object_components parent, const char* field_name,
                   rapidjson::Document::AllocatorType& allocator) const {
            rapidjson::Value field_depends(rapidjson::kObjectType);
            _object.apply(field_depends, allocator);
            parent.dependencies.AddMember(
                rapidjson::Value().SetString(field_name, allocator), field_depends, allocator);
        }

    private:
        object<NamedComponents...> _object;
    };

    /**
     * Base for property modifier detecting required clause
     */
    class required_base {};

    /**
     * Concept: object modifier
     */
    template <typename Component, typename... FieldModifiers>
    class property : public object_modifier {
    public:
        property(const char* n, Component&& v, FieldModifiers... modifiers)
            : _name(n)
            , _value(std::forward<Component>(v))
            , _modifiers(modifiers...) {}

        void apply(object_components parent, rapidjson::Document::AllocatorType& allocator) const {
            rapidjson::Value field_value;
            _value.apply(field_value, allocator);
            parent.properties.AddMember(rapidjson::Value().SetString(_name, allocator), field_value, allocator);
            boost::hana::for_each(_modifiers, [&](auto& modifier) {
                if constexpr (std::is_base_of_v<required_base, std::decay_t<decltype(modifier)>>) {
                    auto itr = parent.object.FindMember("required");
                    if (itr != parent.object.MemberEnd()) {
                        itr->value.PushBack(rapidjson::Value().SetString(_name, allocator), allocator);
                    } else {
                        rapidjson::Value required(rapidjson::kArrayType);
                        required.PushBack(rapidjson::Value().SetString(_name, allocator), allocator);
                        parent.object.AddMember("required", required, allocator);
                    }
                } else {
                    modifier.apply(parent, _name, allocator);
                }
            });
        }

    private:
        const char*                           _name;
        Component                             _value;
        boost::hana::tuple<FieldModifiers...> _modifiers;
    };

    /**
     * Concept: object modifier
     */
    template <typename Component, typename... FieldModifiers>
    class pattern_property : public object_modifier {
    public:
        pattern_property(const char* n, Component&& v, FieldModifiers... modifiers)
            : _name(n)
            , _value(std::forward<Component>(v))
            , _modifiers(modifiers...) {}

        void apply(object_components parent, rapidjson::Document::AllocatorType& allocator) const {
            rapidjson::Value field_value;
            _value.apply(field_value, allocator);
            parent.pattern_properties.AddMember(rapidjson::Value().SetString(_name, allocator), field_value, allocator);
            boost::hana::for_each(_modifiers, [&](auto& modifier) { modifier.apply(parent, _name, allocator); });
        }

    private:
        const char*                           _name;
        Component                             _value;
        boost::hana::tuple<FieldModifiers...> _modifiers;
    };

    /**
     * Concept: object modifier
     */
    template <typename ComponentOrBool>
    class additional_properties : public object_modifier {
    public:
        explicit additional_properties(ComponentOrBool value)
            : _value(value) {}

        void apply(object_components parent, rapidjson::Document::AllocatorType& allocator) const {
            if constexpr (std::is_same_v<bool, ComponentOrBool>) {
                parent.object.AddMember("additionalProperties", rapidjson::Value().SetBool(_value), allocator);
            } else {
                rapidjson::Value comp_value;
                _value.apply(comp_value, allocator);
                parent.object.AddMember("additionalProperties", comp_value, allocator);
            }
        }

    private:
        ComponentOrBool _value;
    };

    /**
     * Concept: object modifier
     */
    template <typename... StringLike>
    class required
        : public object_modifier
        , public required_base {
    public:
        explicit required(StringLike... field_names)
            : _field_names(field_names...) {}

        void apply(object_components parent, rapidjson::Document::AllocatorType& allocator) const {
            auto itr = parent.object.FindMember("required");
            if (itr != parent.object.MemberEnd()) {
                boost::hana::for_each(_field_names, [&](auto& name) {
                    itr->value.PushBack(rapidjson::Value().SetString(name, allocator), allocator);
                });
            } else {
                rapidjson::Value names(rapidjson::kArrayType);
                boost::hana::for_each(_field_names, [&](auto& name) {
                    names.PushBack(rapidjson::Value().SetString(name, allocator), allocator);
                });
                parent.object.AddMember("required", names, allocator);
            }
        }

    private:
        boost::hana::tuple<StringLike...> _field_names;
    };

    /**
     * Concept: object modifier
     */
    class property_names : public object_modifier {
    public:
        explicit property_names(const char* pattern)
            : _pattern(pattern) {}

        void apply(object_components parent, rapidjson::Document::AllocatorType& allocator) const {
            rapidjson::Value names_pattern(rapidjson::kObjectType);
            names_pattern.AddMember("pattern", rapidjson::Value().SetString(_pattern, allocator), allocator);
            parent.object.AddMember("propertyNames", names_pattern, allocator);
        }

    private:
        const char* _pattern;
    };

#define SIZE_MODIFIER(modifier_class_name, modifier_field_name)                                                        \
    class modifier_class_name : public object_modifier {                                                               \
    public:                                                                                                            \
        explicit modifier_class_name(size_t value)                                                                     \
            : _value(value) {}                                                                                         \
        void apply(object_components parent, rapidjson::Document::AllocatorType& allocator) const {                    \
            parent.object.AddMember(#modifier_field_name, rapidjson::Value().SetInt64(_value), allocator);             \
        }                                                                                                              \
                                                                                                                       \
    private:                                                                                                           \
        size_t _value;                                                                                                 \
    }

    SIZE_MODIFIER(min_properties, minProperties);

    SIZE_MODIFIER(max_properties, maxProperties);

#undef SIZE_MODIFIER

}  // namespace rapidmessage::schema
