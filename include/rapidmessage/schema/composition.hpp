#pragma once

#include <boost/hana/for_each.hpp>
#include <boost/hana/tuple.hpp>
#include <rapidjson/document.h>

namespace rapidmessage::schema {

    /**
     * Concept: component
     */

#define COMPOSITION(comp_name, comp_key)                                                                               \
    template <typename... Components>                                                                                  \
    class comp_name {                                                                                                  \
    public:                                                                                                            \
        explicit comp_name(Components... components)                                                                   \
            : _components(components...) {}                                                                            \
                                                                                                                       \
        void apply(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator) const {                     \
            value.SetObject();                                                                                         \
                                                                                                                       \
            rapidjson::Value comps(rapidjson::kArrayType);                                                             \
            boost::hana::for_each(_components, [&](auto& comp) {                                                       \
                rapidjson::Value comp_value;                                                                           \
                comp.apply(comp_value, allocator);                                                                     \
                comps.PushBack(comp_value, allocator);                                                                 \
            });                                                                                                        \
            value.AddMember(#comp_key, comps, allocator);                                                              \
        }                                                                                                              \
                                                                                                                       \
    private:                                                                                                           \
        boost::hana::tuple<Components...> _components;                                                                 \
    }

    COMPOSITION(all_of, allOf);

    COMPOSITION(any_of, anyOf);

    COMPOSITION(one_of, oneOf);

#undef COMPOSITION

#define CONDITIONAL(comp_name, comp_key)                                                                               \
    template <typename Component>                                                                                      \
    class comp_name {                                                                                                  \
    public:                                                                                                            \
        explicit comp_name(Component component)                                                                        \
            : _component(component) {}                                                                                 \
                                                                                                                       \
        void apply(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator) const {                     \
            value.SetObject();                                                                                         \
                                                                                                                       \
            rapidjson::Value subval;                                                                                   \
            _component.apply(subval, allocator);                                                                       \
            value.AddMember(#comp_key, subval, allocator);                                                             \
        }                                                                                                              \
                                                                                                                       \
    private:                                                                                                           \
        Component _component;                                                                                          \
    }

    CONDITIONAL(if_, if);
    CONDITIONAL(then_, then);
    CONDITIONAL(else_, else);
    CONDITIONAL(not_, not);

#undef CONDITIONAL

}  // namespace rapidmessage::schema
