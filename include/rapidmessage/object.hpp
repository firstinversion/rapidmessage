#pragma once

#include "array.hpp"
#include "error.hpp"
#include <boost/preprocessor.hpp>
#include <optional>
#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>
#include <rapidmessage/extract.hpp>
#include <rapidmessage/set.hpp>

namespace rapidmessage::internal {

    using namespace rapidjson;

    template <typename T>
    struct member_extract_val {
        auto operator()(Value& object, const char* field, Document::AllocatorType& allocator) {
            auto itr = object.FindMember(field);
            if (itr == object.MemberEnd()) {
                Value value(kObjectType);
                object.AddMember(Value().SetString(field, allocator), value, allocator);
                return T(object[field], allocator);
            } else {
                return T(itr->value, allocator);
            }
        }
    };

    template <typename T>
    struct member_extract_val<rapidmessage::array<T>> {
        auto operator()(Value& object, const char* field, Document::AllocatorType& allocator) {
            auto itr = object.FindMember(field);
            if (itr == object.MemberEnd()) {
                Value value(kArrayType);
                object.AddMember(Value().SetString(field, allocator), value, allocator);
                return rapidmessage::array<T>(object[field], allocator);
            } else {
                return rapidmessage::array<T>(object[field], allocator);
            }
        }
    };

#define MEM_EXTRACT_IMPL(type)                                                                                         \
    template <>                                                                                                        \
    struct member_extract_val<std::optional<type>> {                                                                   \
        std::optional<type> operator()(Value& object, const char* field, Document::AllocatorType& allocator) {         \
            auto itr = object.FindMember(field);                                                                       \
            if (itr == object.MemberEnd()) {                                                                           \
                return std::nullopt;                                                                                   \
            } else {                                                                                                   \
                return std::optional(rapidmessage::internal::extract_val<type>(itr->value, allocator));                \
            }                                                                                                          \
        }                                                                                                              \
    };

    MEM_EXTRACT_IMPL(bool)
    MEM_EXTRACT_IMPL(float)
    MEM_EXTRACT_IMPL(double)
    MEM_EXTRACT_IMPL(int32_t)
    MEM_EXTRACT_IMPL(int64_t)
    MEM_EXTRACT_IMPL(uint32_t)
    MEM_EXTRACT_IMPL(uint64_t)
    MEM_EXTRACT_IMPL(std::string_view)

    template <>
    struct member_extract_val<std::optional<std::string>> {
        std::optional<std::string_view> operator()(Value& object, const char* field,
                                                   Document::AllocatorType& allocator) {
            auto itr = object.FindMember(field);
            if (itr == object.MemberEnd()) {
                return std::nullopt;
            } else {
                return std::optional(rapidmessage::internal::extract_val<std::string>(itr->value, allocator));
            }
        }
    };

#undef MEM_EXTRACT_IMPL

    template <>
    struct member_extract_val<bool> {
        auto operator()(Value& object, const char* field, Document::AllocatorType& allocator) {
            auto& val = object[field];
            return rapidmessage::internal::extract_val<bool>(val, allocator);
        }
    };

    template <>
    struct member_extract_val<float> {
        auto operator()(Value& object, const char* field, Document::AllocatorType& allocator) {
            auto& val = object[field];
            return rapidmessage::internal::extract_val<float>(val, allocator);
        }
    };

    template <>
    struct member_extract_val<double> {
        auto operator()(Value& object, const char* field, Document::AllocatorType& allocator) {
            auto& val = object[field];
            return rapidmessage::internal::extract_val<double>(val, allocator);
        }
    };

    template <>
    struct member_extract_val<int32_t> {
        auto operator()(Value& object, const char* field, Document::AllocatorType& allocator) {
            auto& val = object[field];
            return rapidmessage::internal::extract_val<int32_t>(val, allocator);
        }
    };

    template <>
    struct member_extract_val<uint32_t> {
        auto operator()(Value& object, const char* field, Document::AllocatorType& allocator) {
            auto& val = object[field];
            return rapidmessage::internal::extract_val<uint32_t>(val, allocator);
        }
    };

    template <>
    struct member_extract_val<int64_t> {
        auto operator()(Value& object, const char* field, Document::AllocatorType& allocator) {
            auto& val = object[field];
            return rapidmessage::internal::extract_val<int64_t>(val, allocator);
        }
    };

    template <>
    struct member_extract_val<uint64_t> {
        auto operator()(Value& object, const char* field, Document::AllocatorType& allocator) {
            auto& val = object[field];
            return rapidmessage::internal::extract_val<uint64_t>(val, allocator);
        }
    };

    template <>
    struct member_extract_val<std::string> {
        auto operator()(Value& object, const char* field, Document::AllocatorType& allocator) {
            auto& val = object[field];
            return rapidmessage::internal::extract_val<std::string>(val, allocator);
        }
    };

    template <>
    struct member_extract_val<std::string_view> {
        auto operator()(Value& object, const char* field, Document::AllocatorType& allocator) {
            auto& val = object[field];
            return rapidmessage::internal::extract_val<std::string_view>(val, allocator);
        }
    };

}  // namespace rapidmessage::internal

namespace rapidmessage {

    class base_message {
    public:
        virtual ~base_message() = default;

        base_message()
            : _document(kObjectType) {}

        explicit base_message(const char* s) { _document.Parse(s); }

        template <typename InputStream>
        explicit base_message(InputStream& s) {
            _document.ParseStream(s);
        }

        virtual const rapidjson::SchemaDocument& schema() const = 0;

        const Document& to_document() const { return _document; }

        template <typename Output>
        void to_json(Output& os) const {
            Writer<Output> writer(os);
            _document.Accept(writer);
        }

        StringBuffer to_json() const {
            StringBuffer buffer;
            to_json(buffer);
            return buffer;
        }

        std::optional<rapidmessage::validation_error> validation_error() noexcept {
            if (_document.HasParseError()) return rapidmessage::validation_error(_document);
            rapidjson::SchemaValidator validator(schema());
            if (!_document.Accept(validator)) return rapidmessage::validation_error(_document, validator);
            return std::nullopt;
        }

        void validate_throw() {
            if (_document.HasParseError()) throw rapidmessage::validation_error(_document);
            rapidjson::SchemaValidator validator(schema());
            if (!_document.Accept(validator)) throw rapidmessage::validation_error(_document, validator);
        }

    protected:
        Document _document;
    };

    class object {
    public:
        virtual ~object() = default;

        object(Value& value, Document::AllocatorType& allocator)
            : _value(value)
            , _allocator(allocator) {}

        Value& to_value() { return _value; }

        void from_value(Value& value) { _value = value; }

    protected:
        Value&                   _value;
        Document::AllocatorType& _allocator;
    };

}  // namespace rapidmessage

#define RAPID_MESSAGE(class_name)                                                                                      \
    class class_name                                                                                                   \
        : public rapidmessage::base_message                                                                            \
        , rapidmessage::object

#define RAPID_MESSAGE_CONSTRUCTORS(class_name)                                                                         \
    class_name()                                                                                                       \
        : base_message()                                                                                               \
        , object(_document, _document.GetAllocator()) {}                                                               \
                                                                                                                       \
    explicit class_name(const char* s)                                                                                 \
        : base_message(s)                                                                                              \
        , object(_document, _document.GetAllocator()) {}                                                               \
                                                                                                                       \
    template <typename InputStream>                                                                                    \
    explicit class_name(InputStream& in)                                                                               \
        : base_message(in)                                                                                             \
        , object(_document, _document.GetAllocator()) {}

#define RAPID_MESSAGE_DEFAULT_VALIDATION                                                                               \
    const rapidjson::SchemaDocument& schema() const override {                                                         \
        namespace s                       = rapidmessage::schema;                                                      \
        static const auto schema_document = s::compile(s::object{});                                                   \
        return schema_document;                                                                                        \
    }

#define RAPID_OBJECT(class_name) class class_name : public rapidmessage::object

#define RAPID_OBJECT_CONSTRUCTORS(class_name)                                                                          \
    class_name(rapidjson::Value& value, rapidjson::Document::AllocatorType& allocator)                                 \
        : object(value, allocator) {}

#define RAPID_PROP_BYVAL(type, name)                                                                                   \
    auto name() { return rapidmessage::internal::member_extract_val<type>()(_value, #name, _allocator); };             \
    void name(type v) {                                                                                                \
        rapidjson::Value value;                                                                                        \
        rapidmessage::internal::set_val(value, std::move(v), _allocator);                                              \
        _value.AddMember(#name, value, _allocator);                                                                    \
    }

#define RAPID_PROP_BYREF(type, name)                                                                                   \
    auto name() { return rapidmessage::internal::member_extract_val<type>()(_value, #name, _allocator); };             \
    void name(type&& v) {                                                                                              \
        rapidjson::Value value;                                                                                        \
        rapidmessage::internal::set_val(value, std::move(v), _allocator);                                              \
        _value.AddMember(#name, value, _allocator);                                                                    \
    }

#define RAPID_PROP_STRING(name)                                                                                        \
    auto name() { return rapidmessage::internal::member_extract_val<std::string_view>()(_value, #name, _allocator); }; \
    void name(std::string_view v) {                                                                                    \
        rapidjson::Value value;                                                                                        \
        rapidmessage::internal::set_val(value, std::move(v), _allocator);                                              \
        _value.AddMember(#name, value, _allocator);                                                                    \
    }

#define RAPID_PROP_OPTIONAL_STRING(name)                                                                               \
    auto name() {                                                                                                      \
        return rapidmessage::internal::member_extract_val<std::optional<std::string_view>>()(                          \
            _value, #name, _allocator);                                                                                \
    };                                                                                                                 \
    void name(std::string_view v) {                                                                                    \
        rapidjson::Value value;                                                                                        \
        rapidmessage::internal::set_val(value, std::move(v), _allocator);                                              \
        _value.AddMember(#name, value, _allocator);                                                                    \
    }
