#pragma once

#include <rapidjson/document.h>
#include <rapidmessage/extract.hpp>
#include <rapidmessage/set.hpp>

namespace rapidmessage {

    using namespace rapidjson;

    template <typename T>
    class array {
    public:
        array(Value& array, Document::AllocatorType& allocator)
            : _array(array)
            , _allocator(allocator) {}

        T operator[](size_t idx) { return internal::extract_val<T>(_array[idx], _allocator); }

        T insert() {
            Value val(kObjectType);
            _array.PushBack(val, _allocator);
            auto itr = _array.End();
            --itr;
            return T(*itr, _allocator);
        }

        void insert(T item) {
            Value val;
            internal::set_val<T>(val, std::move(item), _allocator);
            _array.PushBack(val, _allocator);
        }

        auto size() const { return _array.Size(); }

        bool empty() const { return _array.Empty(); }

    private:
        Value&                   _array;
        Document::AllocatorType& _allocator;
    };

}  // namespace rapidmessage
