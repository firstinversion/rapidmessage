#pragma once

#include <array>
#include <fmt/format.h>
#include <rapidjson/document.h>
#include <rapidjson/schema.h>
#include <rapidjson/stringbuffer.h>
#include <stdexcept>
#include <string>

namespace rapidmessage {

    constexpr auto parse_error_messages = std::array{"No error.",
                                                     "The document is empty.",
                                                     "The document root must not follow by other values.",
                                                     "Invalid _value.",
                                                     "Missing a _name for object member.",
                                                     "Missing a colon after a _name of object member.",
                                                     "Missing a comma or '}' after an object member.",
                                                     "Missing a comma or ']' after an array element.",
                                                     "Incorrect hex digit after \\u escape in string.",
                                                     "The surrogate pair in string is invalid.",
                                                     "Invalid escape character in string.",
                                                     "Missing a closing quotation mark in string.",
                                                     "Invalid encoding in string.",
                                                     "Number too big to be stored in double.",
                                                     "Miss fraction part in number.",
                                                     "Miss exponent in number.",
                                                     "Parsing was terminated.",
                                                     "Unspecific syntax error."};

    class validation_error : public std::exception {
    public:
        explicit validation_error(const rapidjson::Document& doc) {
            _what = fmt::format(
                "Failed to parse json at {}: {}", doc.GetErrorOffset(), parse_error_messages[doc.GetParseError()]);
        }

        validation_error(const rapidjson::Document& doc, const rapidjson::SchemaValidator& validator) {
            rapidjson::StringBuffer isp;
            validator.GetInvalidSchemaPointer().StringifyUriFragment(isp);
            rapidjson::StringBuffer idp;
            validator.GetInvalidDocumentPointer().StringifyUriFragment(idp);

            _what = fmt::format("Failed to validate json: {} violated '{}' rule specified at {}",
                                idp.GetString(),
                                validator.GetInvalidSchemaKeyword(),
                                isp.GetString());
        }

        const char* what() const noexcept override { return _what.data(); }

    private:
        std::string _what;
    };

}  // namespace rapidmessage
