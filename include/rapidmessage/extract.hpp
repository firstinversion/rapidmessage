#pragma once

#include <rapidjson/document.h>
#include <string>
#include <string_view>

namespace rapidmessage::internal {

    using namespace rapidjson;

    template <typename T>
    inline auto extract_val(Value& val, Document::AllocatorType& allocator) {
        return T(val, allocator);
    }

    template <>
    inline auto extract_val<bool>(Value& val, Document::AllocatorType& allocator) {
        assert(val.IsBool());
        return val.GetBool();
    }

    template <>
    inline auto extract_val<float>(Value& val, Document::AllocatorType& allocator) {
        assert(val.IsFloat());
        return val.GetFloat();
    }

    template <>
    inline auto extract_val<double>(Value& val, Document::AllocatorType& allocator) {
        assert(val.IsDouble());
        return val.GetDouble();
    }

    template <>
    inline auto extract_val<int32_t>(Value& val, Document::AllocatorType& allocator) {
        assert(val.IsInt());
        return val.GetInt();
    }

    template <>
    inline auto extract_val<uint32_t>(Value& val, Document::AllocatorType& allocator) {
        assert(val.IsUint());
        return val.GetUint();
    }

    template <>
    inline auto extract_val<int64_t>(Value& val, Document::AllocatorType& allocator) {
        assert(val.IsInt64());
        return val.GetInt64();
    }

    template <>
    inline auto extract_val<uint64_t>(Value& val, Document::AllocatorType& allocator) {
        assert(val.IsUint64());
        return val.GetUint64();
    }

    template <>
    inline auto extract_val<std::string>(Value& val, Document::AllocatorType& allocator) {
        assert(val.IsString());
        return std::string_view(val.GetString(), val.GetStringLength());
    }

    template <>
    inline auto extract_val<std::string_view>(Value& val, Document::AllocatorType& allocator) {
        assert(val.IsString());
        return std::string_view(val.GetString(), val.GetStringLength());
    }

}  // namespace rapidmessage::internal
