#pragma once

#include <boost/preprocessor.hpp>

#define MESSAGE_PLACEHOLDER_FILLER_0(X, Y) ((X, Y)) MESSAGE_PLACEHOLDER_FILLER_1
#define MESSAGE_PLACEHOLDER_FILLER_1(X, Y) ((X, Y)) MESSAGE_PLACEHOLDER_FILLER_0
#define MESSAGE_PLACEHOLDER_FILLER_0_END
#define MESSAGE_PLACEHOLDER_FILLER_1_END

#define MESSAGE_DEFINE_MEMBER(type, name) type BOOST_PP_CAT(_, name);

#define MESSAGE_APPLY_MEMBER(r, _, type_and_name)                                                                      \
    MESSAGE_DEFINE_MEMBER(BOOST_PP_TUPLE_ELEM(2, 0, type_and_name), BOOST_PP_TUPLE_ELEM(2, 1, type_and_name));

#define MESSAGE_DEFINE_ACCESSERS(type, name, member)                                                                   \
    type name() const { return member; }                                                                               \
    void name(type name) { member = name; }

#define MESSAGE_APPLY_ACCESSERS(r, _, type_and_name)                                                                   \
    MESSAGE_DEFINE_ACCESSERS(BOOST_PP_TUPLE_ELEM(2, 0, type_and_name),                                                 \
                             BOOST_PP_TUPLE_ELEM(2, 1, type_and_name),                                                 \
                             BOOST_PP_CAT(_, BOOST_PP_TUPLE_ELEM(2, 1, type_and_name)))

#define MESSAGE_CLASS(class_name, types_and_names)                                                                     \
    class class_name {                                                                                                 \
    public:                                                                                                            \
        BOOST_PP_SEQ_FOR_EACH(MESSAGE_APPLY_ACCESSERS, _,                                                              \
                              BOOST_PP_CAT(MESSAGE_PLACEHOLDER_FILLER_0 types_and_names, _END))                        \
    private:                                                                                                           \
        BOOST_PP_SEQ_FOR_EACH(MESSAGE_APPLY_MEMBER, _,                                                                 \
                              BOOST_PP_CAT(MESSAGE_PLACEHOLDER_FILLER_0 types_and_names, _END))                        \
    };
