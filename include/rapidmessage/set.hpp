#pragma once

#include <rapidjson/document.h>
#include <string>
#include <string_view>

namespace rapidmessage::internal {

    using namespace rapidjson;

    template <typename T>
    inline void set_val(Value& val, T&& item, Document::AllocatorType& allocator) {
        // Maybe make no-op
        // Require descendent of object.
        //    val.Set(item, allocator);
        //    val.Seto
    }

    template <>
    inline void set_val(Value& val, bool&& item, Document::AllocatorType& allocator) {
        val.SetBool(item);
    }

    template <>
    inline void set_val(Value& val, float&& item, Document::AllocatorType& allocator) {
        val.SetFloat(item);
    }

    template <>
    inline void set_val(Value& val, double&& item, Document::AllocatorType& allocator) {
        val.SetDouble(item);
    }

    template <>
    inline void set_val(Value& val, int32_t&& item, Document::AllocatorType& allocator) {
        val.SetInt(item);
    }

    template <>
    inline void set_val(Value& val, uint32_t&& item, Document::AllocatorType& allocator) {
        val.SetUint(item);
    }

    template <>
    inline void set_val(Value& val, int64_t&& item, Document::AllocatorType& allocator) {
        val.SetInt64(item);
    }

    template <>
    inline void set_val(Value& val, uint64_t&& item, Document::AllocatorType& allocator) {
        val.SetUint64(item);
    }

    template <>
    inline void set_val(Value& val, std::string&& item, Document::AllocatorType& allocator) {
        val.SetString(item.data(), item.size(), allocator);
    }

    template <>
    inline void set_val(Value& val, std::string_view&& item, Document::AllocatorType& allocator) {
        val.SetString(item.data(), item.size(), allocator);
    }

}  // namespace rapidmessage::internal
